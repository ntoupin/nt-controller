/*
 * @file NT_Controller.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function definitions.
 */

#include "Arduino.h"
#include "NT_Controller.h"

Controller_t::Controller_t()
{
}

void Controller_t::Configure()
{
	Btn_Up.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Up Button");
	Btn_Left.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Left Button");
	Btn_Down.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Down Button");
	Btn_Right.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Right Button");
	Btn_1.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Button 1");
	Btn_2.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Button 2");
	Btn_3.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Button 3");
	Btn_4.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Button 4");
	Btn_Left_Z1.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Z1 Button Left");
	Btn_Left_Z2.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Z2 Button Left");
	Btn_Right_Z1.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Z1 Button Right");
	Btn_Right_Z2.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Z1 Button Right");
	Btn_Select.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Select Button");
	Btn_Start.Configure(0, 1, 0, 1, INPUT, DIGITAL, "Start Button");
	Joy_Btn_Left.Configure(0, 1024, 0, 1, INPUT, ANALOG, "Joystick Button Left");
	Joy_Btn_Right.Configure(0, 1024, 0, 1, INPUT, ANALOG, "Joystick Button Right");
	Joy_Ana_Left_X.Configure(0, 1023, 0, 1023, INPUT, ANALOG, "Joystick Left X Axis");
	Joy_Ana_Left_Y.Configure(0, 1023, 0, 1023, INPUT, ANALOG, "Joystick Left Y Axis");
	Joy_Ana_Right_X.Configure(0, 1023, 0, 1023, INPUT, ANALOG, "Joystick Right X Axis");
	Joy_Ana_Right_Y.Configure(0, 1023, 0, 1023, INPUT, ANALOG, "Joystick Right Y Axis");
	Vib_Motor.Configure(0, 1, 0, 1, OUTPUT, DIGITAL, "Vibration Motor");
	Com_Led.Configure(0, 1, 0, 1, OUTPUT, DIGITAL, "Communication Led");
}

void Controller_t::Attach()
{
	Btn_Up.Attach(5);
	Btn_Left.Attach(6);
	Btn_Down.Attach(7);
	Btn_Right.Attach(8);
	Btn_1.Attach(9);
	Btn_2.Attach(11);
	Btn_3.Attach(12);
	Btn_4.Attach(10);
	Btn_Left_Z1.Attach(15);
	Btn_Left_Z2.Attach(16);
	Btn_Right_Z1.Attach(13);
	Btn_Right_Z2.Attach(14);
	Btn_Select.Attach(3);
	Btn_Start.Attach(4);
	Joy_Btn_Left.Attach(1);
	Joy_Btn_Right.Attach(0);
	Joy_Ana_Left_X.Attach(5);
	Joy_Ana_Left_Y.Attach(4);
	Joy_Ana_Right_X.Attach(3);
	Joy_Ana_Right_Y.Attach(2);
	Vib_Motor.Attach(2);
	Com_Led.Attach(17);
}

void Controller_t::Init()
{
	Btn_Up.Init();
	Btn_Left.Init();
	Btn_Down.Init();
	Btn_Right.Init();
	Btn_1.Init();
	Btn_2.Init();
	Btn_3.Init();
	Btn_4.Init();
	Btn_Left_Z1.Init();
	Btn_Left_Z2.Init();
	Btn_Right_Z1.Init();
	Btn_Right_Z2.Init();
	Btn_Select.Init();
	Btn_Start.Init();
	Joy_Btn_Left.Init();
	Joy_Btn_Right.Init();
	Joy_Ana_Left_X.Init();
	Joy_Ana_Left_Y.Init();
	Joy_Ana_Right_X.Init();
	Joy_Ana_Right_Y.Init();
	Vib_Motor.Init();
	Com_Led.Init();

	Enable = TRUE;
}

void Controller_t::Get()
{
	if (Enable == TRUE)
	{
		Btn_Up.Get();
		Btn_Left.Get();
		Btn_Down.Get();
		Btn_Right.Get();
		Btn_1.Get();
		Btn_2.Get();
		Btn_3.Get();
		Btn_4.Get();
		Btn_Left_Z1.Get();
		Btn_Left_Z2.Get();
		Btn_Right_Z1.Get();
		Btn_Right_Z2.Get();
		Btn_Select.Get();
		Btn_Start.Get();
		Joy_Btn_Left.Get();
		Joy_Btn_Right.Get();
		Joy_Ana_Left_X.Get();
		Joy_Ana_Left_Y.Get();
		Joy_Ana_Right_X.Get();
		Joy_Ana_Right_Y.Get();
	}
}

void Controller_t::Set()
{
	if (Enable == TRUE)
	{
		Vib_Motor.Set();
		Com_Led.Set();
	}
}

void Controller_t::Deinit()
{
	Btn_Up.Deinit();
	Btn_Left.Deinit();
	Btn_Down.Deinit();
	Btn_Right.Deinit();
	Btn_1.Deinit();
	Btn_2.Deinit();
	Btn_3.Deinit();
	Btn_4.Deinit();
	Btn_Left_Z1.Deinit();
	Btn_Left_Z2.Deinit();
	Btn_Right_Z1.Deinit();
	Btn_Right_Z2.Deinit();
	Btn_Select.Deinit();
	Btn_Start.Deinit();
	Joy_Btn_Left.Deinit();
	Joy_Btn_Right.Deinit();
	Joy_Ana_Left_X.Deinit();
	Joy_Ana_Left_Y.Deinit();
	Joy_Ana_Right_X.Deinit();
	Joy_Ana_Right_Y.Deinit();
	Vib_Motor.Deinit();
	Com_Led.Deinit();

	Enable = FALSE;
}

long Controller_t::Checksum()
{
	long Checksum = 0x1234;
	Checksum ^= Btn_Up.Value_Op;
	Checksum ^= Btn_Left.Value_Op;
	Checksum ^= Btn_Down.Value_Op;
	Checksum ^= Btn_Right.Value_Op;
	Checksum ^= Btn_1.Value_Op;
	Checksum ^= Btn_2.Value_Op;
	Checksum ^= Btn_3.Value_Op;
	Checksum ^= Btn_4.Value_Op;
	Checksum ^= Btn_Left_Z1.Value_Op;
	Checksum ^= Btn_Left_Z2.Value_Op;
	Checksum ^= Btn_Right_Z1.Value_Op;
	Checksum ^= Btn_Right_Z2.Value_Op;
	Checksum ^= Btn_Select.Value_Op;
	Checksum ^= Btn_Start.Value_Op;
	Checksum ^= Joy_Btn_Left.Value_Op;
	Checksum ^= Joy_Btn_Right.Value_Op;
	Checksum ^= Joy_Ana_Left_X.Value_Op;
	Checksum ^= Joy_Ana_Left_Y.Value_Op;
	Checksum ^= Joy_Ana_Right_X.Value_Op;
	Checksum ^= Joy_Ana_Right_Y.Value_Op;
	Checksum ^= Vib_Motor.Value_Op;
	Checksum ^= Com_Led.Value_Op;

	return Checksum;
}