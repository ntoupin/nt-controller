/*
 * @file NT_Control.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Controller function declarations.
 */

#ifndef NT_Control_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Control_h

enum Type_t
{
	UNDEFINED = 1,
	DIGITAL = 2,
	ANALOG = 3
};

class Control_t
{
public:
	bool Enable = FALSE;	
	int Value_Sp = 0;
	int Value_Op = 0;
	void Configure(int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op, int Direction, Type_t Type, String Name);
	void Attach(int Pin);
	void Init();
	void Get();
	void Set();
	void Toggle();
	void Deinit();
	String Print();

private:
	String _Name;
	int _Pin;
	int _Direction = -1;
	Type_t _Type = UNDEFINED;
	int _Minimum_Op = 0;
	int _Maximum_Op = 0;
	int _Minimum_Sp = 0;
	int _Maximum_Sp = 0;
};

#endif