/*
 * @file NT_Control.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function definitions.
 */

#include "Arduino.h"
#include "NT_Control.h"

void Control_t::Configure(int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op, int Direction, Type_t Type, String Name)
{
	_Minimum_Sp = Minimum_Sp;
	_Maximum_Sp = Maximum_Sp;
	_Minimum_Op = Minimum_Op;
	_Maximum_Op = Maximum_Op;
	_Direction = Direction;
	_Type = Type;
	_Name = Name;
}

void Control_t::Attach(int Pin)
{
	_Pin = Pin;

	if (_Direction == INPUT)
	{
		pinMode(_Pin, INPUT);
	}
	else if (_Direction == OUTPUT)
	{
		pinMode(_Pin, OUTPUT);
	}
}

void Control_t::Init()
{
	Enable = TRUE;
}

void Control_t::Get()
{
	if (Enable == TRUE)
	{
		if (_Direction == INPUT)
		{
			if (_Type == DIGITAL)
			{
				Value_Sp = digitalRead(_Pin);
				Value_Op = map(Value_Sp, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op);
			}
			else if (_Type == ANALOG)
			{
				Value_Sp = analogRead(_Pin);
				Value_Op = map(Value_Sp, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op);
			}
		}
	}
}

void Control_t::Set()
{
	if (Enable == TRUE)
	{
		if (_Direction == OUTPUT)
		{
			if (_Type == DIGITAL)
			{
				digitalWrite(_Pin, (bool)Value_Op);
			}
		}
	}
}

void Control_t::Toggle()
{
	if (Enable == TRUE)
	{
		if (_Direction == OUTPUT)
		{
			if (_Type == DIGITAL)
			{
				if (Value_Op == TRUE)
				{
					digitalWrite(_Pin, FALSE);
				}
				if (Value_Op == FALSE)
				{
					digitalWrite(_Pin, TRUE);
				}
			}
		}
	}
}

void Control_t::Deinit()
{
	Enable = FALSE;
}

String Control_t::Print()
{
	return String(String(Value_Op) + " - ");
}