/*
 * @file NT_Controller.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Controller function declarations.
 */

#ifndef NT_Controller_h

#include "Arduino.h"
#include "NT_Control.h"

#define TRUE 1
#define FALSE 0

#define BYTE_QTY_SEND 8
#define BYTE_QTY_RECEIVE 3

#define CHECKSUM_QTY 10

#define NT_Controller_h

class Controller_t
{
public:
	Controller_t();
	bool Enable = FALSE;
	Control_t Btn_Up;
	Control_t Btn_Left;
	Control_t Btn_Down;
	Control_t Btn_Right;
	Control_t Btn_1;
	Control_t Btn_2;
	Control_t Btn_3;
	Control_t Btn_4;
	Control_t Btn_Left_Z1;
	Control_t Btn_Left_Z2;
	Control_t Btn_Right_Z1;
	Control_t Btn_Right_Z2;
	Control_t Btn_Select;
	Control_t Btn_Start;
	Control_t Joy_Btn_Left;
	Control_t Joy_Btn_Right;
	Control_t Joy_Ana_Left_X;
	Control_t Joy_Ana_Left_Y;
	Control_t Joy_Ana_Right_X;
	Control_t Joy_Ana_Right_Y;
	Control_t Vib_Motor;
	Control_t Com_Led;	
	void Configure();
	void Attach();
	void Init();
	void Get();
	void Set();
	void Deinit();
	long Checksum();

private:
};

#endif