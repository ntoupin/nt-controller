/*
   @file Controller.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of controller utilisation.
*/

#include <NT_Controller.h>

Controller_t Controller1;

void setup()
{
  Controller1.Configure();
  Controller1.Init();
}

void loop()
{
  Controller1.Get();
  Controller1.Set();
}
